# HotelBot 
## Chatbot for Hotels

### -Language: C# (.NET)
### -Microsoft Bot Framework
### -LUIS (Azure Cognitive Services)

***

## Functionalities


| Functionality | Description   | Importance  |
| ------------- |:-------------:| :----------:|
| Functionality | right-aligned |      		  |
| Functionality | centered      |        	  |
| Functionality | are neat      |          	  |


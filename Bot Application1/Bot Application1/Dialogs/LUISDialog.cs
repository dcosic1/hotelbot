﻿using Bot_Application1.Models;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using Microsoft.Bot.Builder.Luis;
using Microsoft.Bot.Builder.Luis.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Bot_Application1.Dialogs
    {
    [LuisModel("e93670cd-5776-41f5-8331-a3196cb9242b", "1ef1fbebf3f74be5aa90d4e8f9396dfe")]
    [Serializable]
    public class LUISDialog : LuisDialog<RoomReservation>
        {

        private readonly BuildFormDelegate<RoomReservation> ReserveRoom;

        public LUISDialog (BuildFormDelegate<RoomReservation> reserveRoom)
            {
            this.ReserveRoom = reserveRoom;
            }

        [LuisIntent("")]
        public async Task None (IDialogContext context, LuisResult result)
            {
            await context.PostAsync("I am sorry, but either I don't know what you mean or I can not help you with that.");
            context.Wait(MessageReceived);
            }

        [LuisIntent("Greeting")]
        public async Task Greeting (IDialogContext context, LuisResult result)
            {
            context.Call(new GreetingDialog(), Callback);
            }

        private async Task Callback (IDialogContext context, IAwaitable<object> result)
            {
            context.Wait(MessageReceived);
            }

        [LuisIntent("BasicInformations")]
        public async Task BasicInformations (IDialogContext context, LuisResult result)
            {
            await context.PostAsync("The distinctive sail-shaped silhouette of Burj Al Arab Jumeirah is more than just a stunning hotel, it is a symbol of modern Dubai. Yet for all the wonder this stunning structure provides when you finally see it in person, it is the service within that really makes the Burj Al Arab Jumeirah so extraordinary. Repeatedly voted the world's most luxurious hotel, this magnificent destination offers you the finest service and experiences throughout - right down to an optional chauffeur-driven Rolls-Royce, helicopter trips from an iconic helipad, private beach access, luxury leisure on a breathtaking terrace with pools and cabanas as well as some of the world’s best dining venues, including the highly acclaimed Nathan Outlaw at Al Mahara. Burj Al Arab Jumeirah suite-only accommodation offers discreet check-in within your suite, a private reception on every floor and a host of personal butlers, each a warm messenger of our unparalleled hospitality. Come and experience it for yourself.");
            context.Wait(MessageReceived);
            }


        [LuisIntent("Reservation")]
        public async Task RoomReservation (IDialogContext context, LuisResult result)
            {
            var enrollmentForm = new FormDialog<RoomReservation>(new RoomReservation(), this.ReserveRoom, FormOptions.PromptInStart);
            context.Call<RoomReservation>(enrollmentForm, Callback);
            }

        [LuisIntent("CancelReservation")]
        public async Task CancelReservation (IDialogContext context, LuisResult result)
            {
            //context.Call(new CancelReservationDialog(), Callback);
            await context.PostAsync("Reservation that was connected with your username has been successfully canceled. \n We hope that you will visit us soon!");
            context.Wait(MessageReceived);
            }


        [LuisIntent("QueryAmenities")]
        public async Task QueryAmenities (IDialogContext context, LuisResult result)
            {
            foreach (var entity in result.Entities.Where(Entity => Entity.Type == "Amenity"))
                {
                var value = entity.Entity.ToLower();
                if (value == "pool" || value == "gym" || value == "wifi" || value == "towels")
                    {
                    await context.PostAsync("Yes, we have that!");
                    context.Wait(MessageReceived);
                    return;
                    }
                else
                    {
                    await context.PostAsync("I am sorry, but we don't have that.");
                    context.Wait(MessageReceived);
                    return;
                    }
                }
            await context.PostAsync("I am sorry we don't have that.");
            context.Wait(MessageReceived);
            return;
            }




        }
    }
﻿using Microsoft.Bot.Builder.FormFlow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bot_Application1.Models
    {

    public enum BedSizeOptions
        {
        King,
        Queen,
        Single,
        Double
        }

    public enum AmenitiesOptions
        {
        Kitchen,
        ExtraTowels,
        GymAccess,
        Wifi,
        Pool
        }

    [Serializable]
    public class RoomReservation
        {
        // nullable atribute ? - opcija omogućava da se ne bira vrsta kreveta
        // Raspored atributa je bitan zbog toga sto MicrosoftBotFramework postavlja pitanja po redoslijedu, pa zbog toga prvo ide check in a zatim dani boravka
        public BedSizeOptions? BedSize;
        public int? NumberOfOccupants;
        public DateTime? CheckInDate;
        public int? NumberOfDaysToStay;
        public List<AmenitiesOptions> Amenities;

        public static IForm<RoomReservation> BuildForm ()
            {
            // .Message prikazuje poruku svaki put prilikom pokretanja forme
            return new FormBuilder<RoomReservation>().Message("Let us help you with your reservation!").Build();
            }
        }
    }